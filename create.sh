#!/bin/bash

terraform -chdir=./terraform init 
terraform -chdir=./terraform apply --auto-approve --var-file="variables.tfvars"

#Allow hosts to boot properly, otherwise apt locks can occur
echo 'Waiting extra 10 seconds for hosts to fully become online'
sleep 10

ansible-playbook --vault-password-file ~/.ansible/vaultpass main.yml

git -C ./kubernetes-inventory add .
git -C ./kubernetes-inventory commit -am 'inventory update after terraform'
git -C ./kubernetes-inventory push