variable proxmox {
    type = object({
        host                = string
        api_token_id        = string
        api_token_secret    = string
    })
}

variable ssh_public_key {
    default = "~/.ssh/peter.pub"
}

variable ssh_private_key {
    default = "~/.ssh/peter"
}

variable ssh_user {
    default = "peter"
}

variable proxmox_host {
    default = "proxmox"
}

variable  template_name {
    default = "ubuntu-22-template"
}

variable master_node {
    type = object({
        ipadresses      = list(string)
        gateway         = string
        count           = number
    })
}

variable worker_node {
    type = object({
        ipadresses      = list(string)
        gateway         = string
        count           = number
    })
}

