[masters]
${k3s_master_ip}

[workers]
${k3s_worker_ip}

[k3s_cluster:children]
masters
workers

[all:vars]
ansible_connection=ssh
ansible_user=${ssh_user}
ansible_ssh_private_key_file=${ssh_privkey}