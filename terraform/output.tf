output "master_ips" {
  value = ["${proxmox_vm_qemu.master_nodes.*.default_ipv4_address}"]
}

output "node_ips" {
  value = ["${proxmox_vm_qemu.worker_nodes.*.default_ipv4_address}"]
}

data "template_file" "ansible_hosts" {
  template = file("./templates/ansible-hosts.tpl")
  vars = {
    k3s_master_ip = "${join("\n", [for instance in proxmox_vm_qemu.master_nodes : join("", [instance.default_ipv4_address])])}"
    k3s_worker_ip = "${join("\n", [for instance in proxmox_vm_qemu.worker_nodes : join("", [instance.default_ipv4_address])])}"
    ssh_user      = var.ssh_user
    ssh_privkey   = var.ssh_private_key
  }
}

resource "local_file" "hosts_file" {
  content  = data.template_file.ansible_hosts.rendered
  filename = "../kubernetes-inventory/hosts.ini"
}