# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/local" {
  version = "2.2.2"
  hashes = [
    "h1:5UYW2wJ320IggrzLt8tLD6MowePqycWtH1b2RInHZkE=",
    "zh:027e4873c69da214e2fed131666d5de92089732a11d096b68257da54d30b6f9d",
    "zh:0ba2216e16cfb72538d76a4c4945b4567a76f7edbfef926b1c5a08d7bba2a043",
    "zh:1fee8f6aae1833c27caa96e156cf99a681b6f085e476d7e1b77d285e21d182c1",
    "zh:2e8a3e72e877003df1c390a231e0d8e827eba9f788606e643f8e061218750360",
    "zh:719008f9e262aa1523a6f9132adbe9eee93c648c2981f8359ce41a40e6425433",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:9a70fdbe6ef955c4919a4519caca116f34c19c7ddedd77990fbe4f80fe66dc84",
    "zh:abc412423d670cbb6264827fa80e1ffdc4a74aff3f19ba6a239dd87b85b15bec",
    "zh:ae953a62c94d2a2a0822e5717fafc54e454af57bd6ed02cd301b9786765c1dd3",
    "zh:be0910bdf46698560f9e86f51a4ff795c62c02f8dc82b2b1dab77a0b3a93f61e",
    "zh:e58f9083b7971919b95f553227adaa7abe864fce976f0166cf4d65fc17257ff2",
    "zh:ff4f77cbdbb22cc98182821c7ef84dce16298ab0e997d5c7fae97247f7a4bcb0",
  ]
}

provider "registry.terraform.io/hashicorp/template" {
  version = "2.2.0"
  hashes = [
    "h1:94qn780bi1qjrbC3uQtjJh3Wkfwd5+tTtJHOb7KTg9w=",
    "zh:01702196f0a0492ec07917db7aaa595843d8f171dc195f4c988d2ffca2a06386",
    "zh:09aae3da826ba3d7df69efeb25d146a1de0d03e951d35019a0f80e4f58c89b53",
    "zh:09ba83c0625b6fe0a954da6fbd0c355ac0b7f07f86c91a2a97849140fea49603",
    "zh:0e3a6c8e16f17f19010accd0844187d524580d9fdb0731f675ffcf4afba03d16",
    "zh:45f2c594b6f2f34ea663704cc72048b212fe7d16fb4cfd959365fa997228a776",
    "zh:77ea3e5a0446784d77114b5e851c970a3dde1e08fa6de38210b8385d7605d451",
    "zh:8a154388f3708e3df5a69122a23bdfaf760a523788a5081976b3d5616f7d30ae",
    "zh:992843002f2db5a11e626b3fc23dc0c87ad3729b3b3cff08e32ffb3df97edbde",
    "zh:ad906f4cebd3ec5e43d5cd6dc8f4c5c9cc3b33d2243c89c5fc18f97f7277b51d",
    "zh:c979425ddb256511137ecd093e23283234da0154b7fa8b21c2687182d9aea8b2",
  ]
}

provider "registry.terraform.io/telmate/proxmox" {
  version     = "2.9.9"
  constraints = "2.9.9"
  hashes = [
    "h1:wYsx6S2JJhVAk2DKfEkKgchAwnG9tNOZ+37rpLd7XkM=",
    "zh:01200b0a876f54768437a441918d18140bd35d8362537a6904303ccf4dcacc18",
    "zh:029db13850dab46e16f00f8c9bb5d6dda3c4cd6484071e664b3c537494d95653",
    "zh:04ac3514cd5c8a16beec6ef955063d59780bf10f86768b9cf5a4b2ecca916ffb",
    "zh:0a199bdf2cedb17689b7891b33788a36bdaf902ce04f2996d18e8b2bb9961e6b",
    "zh:13a31f62729e08007439ab1b9fdfe74c761e213f0c3aab06bc451c89bdc01159",
    "zh:31662b4056c6856ea02e6f14bb6cbe3edb3097a90cd215c2c77f46aac401bcac",
    "zh:3d8c3d71cd89a8e1646d7f5be4b360dad15cfdf10e396425dde51aa01b556b5c",
    "zh:599a475957a5b49720c344605f604c4c907d1798a3483f32f0daf634de19ebbf",
    "zh:658ceec83f80b3c2f992535b6566a91f49bd980e0ebd1488e61aeb063466fa07",
    "zh:78d94953a2d880c6ea06449a8d679cc7e90f098fbb2e3e3ea1a8f43ed0797bc3",
    "zh:a1c3679240ad8375501246651f10fe4265c45e118eabaa479e12feeeaa689fb8",
    "zh:aef9d52798198a13e14791bce7dec62b97387ce5ac782818aa251729d9207644",
    "zh:e1756079eda71d1ec38587a74e6bc820bfd46740b6aa57c4ae4568a6ba20cd12",
    "zh:e6efe3dfbd5c6f145bf4daa21e7d1e260440849b4b2bcbdcd79010d9d8964ffc",
  ]
}
