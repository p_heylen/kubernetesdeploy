resource "proxmox_vm_qemu" "master_nodes" {
    count = var.master_node.count
    name = "kubernetes-master-${count.index + 1}" 
        
    target_node = var.proxmox_host
    clone = var.template_name

    os_type = "cloud-init"
    agent = 1
    onboot = true
    
    #memory and min memory settings
    memory  = 8192
    balloon = 1024

    cores = 4

    disk {
        size = "10G"
        type = "scsi"
        storage = "local-lvm"
    }

    network {
        bridge      = "vmbr0"
        model       = "virtio"
    }

    ipconfig0 = "ip=${var.master_node.ipadresses[count.index]},gw=${var.master_node.gateway}"
    
    sshkeys = file("${var.ssh_public_key}")
    ciuser = "peter"
}

resource "proxmox_vm_qemu" "worker_nodes" {
    count = var.worker_node.count
    name = "kubernetes-worker-${count.index + 1}" 
        
    target_node = var.proxmox_host
    clone = var.template_name

    os_type = "cloud-init"
    agent = 1
    onboot = true

    #memory and min memory settings
    memory  = 8192
    balloon = 1024

    cores = 4

    disk {
        size = "10G"
        type = "scsi"
        storage = "local-lvm"
    }

    network {
        bridge      = "vmbr0"
        model       = "virtio"
    }

    ipconfig0 = "ip=${var.worker_node.ipadresses[count.index]},gw=${var.worker_node.gateway}"

    sshkeys = file("${var.ssh_public_key}")
    ciuser = "peter"
}