terraform {
  required_providers {
    proxmox = {
      source = "telmate/proxmox"
      version = "2.9.9"
    }
  }
}

provider "proxmox" {
  pm_api_url = "https://${var.proxmox.host}:8006/api2/json"
  pm_api_token_id = var.proxmox.api_token_id 
  pm_api_token_secret = var.proxmox.api_token_secret 

  # leave tls_insecure set to true unless you have your proxmox SSL certificate situation fully sorted out (if you do, you will know)
  pm_tls_insecure = true

  # remove these next lines to disable debug logging
  pm_log_enable = true
  pm_log_file   = "terraform-plugin-proxmox.log"
  pm_debug      = false
  pm_log_levels = {
    _default    = "debug"
    _capturelog = ""
  }
}



